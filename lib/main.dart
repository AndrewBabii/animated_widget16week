import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  int _counter = 0;

  AnimationController controller;
  AnimationController btnController;
  bool fadeVar = false;


  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      duration: const Duration(seconds: 10),
      vsync: this,
    )..repeat();
    btnController = AnimationController(
      duration: const Duration(seconds: 10),
      vsync: this,
      value: 50,
    )..repeat();
  }

  void _incrementCounter() {
    setState(() {
      firstWidget = secondWidget;
      _counter++;
      fadeVar = !fadeVar;
    });
  }

  Widget firstWidget = Container(
    height: 100,
    width: 50,
    color: Colors.yellow,
    key: ValueKey(1),
  );

  Widget secondWidget = Container(
    height: 50,
    width: 100,
    color: Colors.purpleAccent,
    key: ValueKey(2),
  );

  @override
  Widget build(BuildContext context) {
    final animation = Tween(begin: 0.0, end: 2 * pi).animate(controller);

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 100,
              width: 100,
              child: AnimatedBuilder(
                  animation: animation,
                  child: Text(
                    'You have ',
                  ),
                  builder: (context, child) {
                    return Transform.rotate(
                      angle: animation.value,
                      child: child,
                    );
                  }),
            ),
MyAnimatedButton(width: btnController),
            AnimatedSwitcher(
              duration: const Duration(seconds: 5),
              transitionBuilder: (Widget child, Animation<double> animation) => ScaleTransition(
                child: child,
                scale: animation,
              ),
              child: firstWidget,
            ),
            AnimatedCrossFade(
              firstChild: Text('bla bla'),
              secondChild: Text('ahoy'),
              crossFadeState: fadeVar ? CrossFadeState.showFirst : CrossFadeState.showSecond,
              duration: const Duration(seconds: 2),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}

class MyAnimatedButton extends AnimatedWidget{
  const MyAnimatedButton({width}) : super(listenable: width);

  Animation<double> get width => listenable;

  @override
  Widget build(BuildContext context) {
    return OutlineButton(onPressed: (){},
      borderSide: BorderSide(width: width.value),
      child: Container(
        height: 50,
        width: 50,
      ),
    );
  }
}
